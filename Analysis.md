### Enhancements:

1. Use SSL on ingress
2. Change mySQL to stateful set to keep pod state and prevent restart problems
3. Separate config from the application
4. Remove secrets from the repository
5. Mount the wordpress uploads folder to keep the data on persistent volume
6. Make a replica for mySQL
7. Take periodic backups of the mySQL db


## Notice:

1. Start minikube on normal user then run the `build_image.sh` to build the docker image
2. apply the files in the kubernetes directory to the cluster using kubectl
3. To demo the pipeline start minikube in the jenkins user to set the context
4. Make a pipeline job on jenkins and deploy the pipeline
5. The last step in the pipline will fail on minikube on jenkins user as the jenkins user cant redirect traffic on port 80 to the vm
