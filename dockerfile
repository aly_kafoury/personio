FROM php:7.2-apache
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libxml2-dev \
    && docker-php-ext-install -j$(nproc) iconv \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd mbstring xml xmlrpc soap intl zip mysqli

ENV APACHE_DOCUMENT_ROOT /var/www/html/wordpress
COPY ./wordpress /var/www/html/wordpress
RUN rm /etc/apache2/sites-enabled/000-default.conf
COPY ./wordpress.conf /etc/apache2/sites-enabled/
